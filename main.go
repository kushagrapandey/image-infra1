package main

import (
	"fmt"
	"goproj/tools"
	"io"
	"net/http"
	"github.com/h2non/bimg"
)

var projectId string = "staging-bikayi";
var bucketName string = "bikayi-stg-cdn";

func compress(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path!="/" {
		fmt.Println("Invalid path: ", r.URL.Path);
		http.NotFound(w, r);
        return;
	}

	query := r.URL.Query();
	fileName, ok := query["fileName"];

	if !ok {
		fmt.Println("filename parameter not found");
		http.NotFound(w, r);
		return;
	}

	imageParams := tools.GetValidParams(w,r);
	file,err := tools.GetFile(projectId,bucketName,fileName[0]);

	if err!=nil {
		fmt.Println(err);
		http.NotFound(w,r);
		return;
	}



	p := make([]byte, 5000);
	for{
		n, err := file.Read(p);
		if(err==io.EOF){
			break;
		}
		if(err!=nil){
			fmt.Println(err);
			return;
		}
		if(n>0){
			newImage, err := bimg.NewImage(p[:n]).Rotate(90);
			if err != nil {
				fmt.Println(err);
				http.NotFound(w,r);
				return;
			}
			w.Write(newImage)
			
//			w.Write(p[:n]);
		}
	}
	fmt.Println(imageParams);

}


func main() {
	http.HandleFunc("/",compress);
	http.ListenAndServe(":8080",nil);
}