package tools;

import (
	"context"
	"fmt"
	"cloud.google.com/go/storage"
)

func GetFile(projectId string, bucketName string, fileName string) (*storage.Reader,error) {
	ctx := context.Background();
	client, err := storage.NewClient(ctx)
	if err != nil {
		fmt.Println(err);
		return nil,err;
	}
	defer client.Close();
	fmt.Println(bucketName);
	bucket := client.Bucket(bucketName);
	bucket.UserProject(projectId);
	file,err := bucket.Object(fileName).NewReader(ctx);

	if err!=nil {
		return nil,err;
	}

	return file,nil;
}