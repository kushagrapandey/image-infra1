package tools;

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)

func GetValidParams(w http.ResponseWriter, r *http.Request) ImageParams {
	query := r.URL.Query();
	var height,width int;

	height, err1 :=  strconv.Atoi(query.Get("height"));
	width, err2 := strconv.Atoi(query.Get("width"));
	var format string = query.Get("format");


	if err1 != nil {
		height = 0;
	}
	if err2 != nil {
		width = 0;
	}


	validformat := map[string]bool{
		"jpeg": true,
		"webp": true,
		"png" : true,
	};

	val, ok := validformat[format];
	if !ok {
		fmt.Println(val);
		format = "jpeg";
	}
	
	height = int(math.Min(10000,float64(height)));
	width = int(math.Min(10000,float64(width)));

	fmt.Println("width :", width, " height :", height);

	image := ImageParams{
		height: height,
		width: width,
		format: format,
	}

	fmt.Println("mm ",height,width,format);
	return image;
	
}
