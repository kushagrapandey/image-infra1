package tools;

type ImageParams struct {  
    height   int;
    width int;
	format string;
}